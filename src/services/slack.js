'use strict';

const logger = require('../utils/logger');
const axios = require('axios');
const CONFIG = require('../config');

const service = {};

service.notificationToChannel = function (channelName, message) {

    logger.info('notificationByChannel call [' + channelName + '] : ' + message);
    axios.post(CONFIG.SLACK_NOTIFICATION_PREFIX_URL, message, {
        params: {
            token: CONFIG.ENV_INFO.slack.token,
            channel: channelName
        }
    });

};

module.exports = service;
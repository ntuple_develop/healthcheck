const dbService = require('./db');
const diff = require('deep-diff').diff;
const CONFIG = require('../config');
const STATIC_CONFIG = require('../config/static');
const logger = require('../utils/logger');
const axios = require('axios');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const slackService = require('./slack');
const commonUtil = require('../utils/common');
// const errorInstanceRepository = require('../utils/errorInstanceRepository');

const service = {};

// 실시간 모니터링 종류의 요청들 작업
service.startMonitoringRealTime = function () {
    logger.info('startMonitoringRealTime call');
    dbService.selectQueryById('getTaskListAll', STATIC_CONFIG.MONITORING_TYPE_REAL)
        .then((result) => {

            result.forEach(taskInfo => {

                axios.get(taskInfo.url, { timeout: CONFIG.TIMEOUT_MAX_MS })
                    .then((successResponse) => {

                        // html dom check
                        if (taskInfo.request_type === STATIC_CONFIG.REQUEST_TYPE_ELEMENT
                            && successResponse.data && taskInfo.dom_selector) {
                            let dom = new JSDOM(successResponse.data);
                            let selectorResult = dom.window.document.querySelector(taskInfo.dom_selector);
                            if (!selectorResult) {
                                let slackSendMessage = commonUtil.convertSlackSendMessage(successResponse, taskInfo);
                                slackService.notificationToChannel(taskInfo.slack_channel_name, slackSendMessage);
                            }
                        } else if (taskInfo.json_response_skeleton) {
                            // rest api check
                            let successJsonSkeleton = JSON.parse(taskInfo.json_response_skeleton);
                            let differences = diff(successJsonSkeleton, successResponse.data);
                            let diffResult = [];
                            if (differences && differences.length) {
                                for (let index = 0; index < differences.length; index++) {
                                    let diffInfo = differences[index];
                                    if (diffInfo.kind === 'D') {
                                        diffResult.push(diffInfo.path.join('.'));
                                    }
                                }
                            }
                            // 등록된 json 응답구조와 실제 데이터의 json 응답 구조가 틀린 경우
                            if (diffResult.length) {
                                let diffResultString = diffResult.join();
                                let slackSendMessage = commonUtil.convertSlackSendMessage(successResponse, taskInfo, diffResultString);
                                slackService.notificationToChannel(taskInfo.slack_channel_name, slackSendMessage);
                            }
                        }

                    }).catch((failResponse) => {
                        let slackSendMessage = commonUtil.convertSlackSendMessage(failResponse, taskInfo);
                        slackService.notificationToChannel(taskInfo.slack_channel_name, slackSendMessage);
                    });

            });

        });

};

// 일일 체크 모니터링 종류의 요청들 작업
service.startMonitoringDaily = function () {
    logger.info('startMonitoringDaily call');
    // dbService.selectQueryById('getTaskById', 23)
    //     .then((result) => {
    //         result.forEach(taskInfo => {
    //             axios.get(taskInfo.url, { timeout: CONFIG.TIMEOUT_MAX_MS })
    //                 .catch(() => {
    //                     errorInstanceRepository.create(taskInfo);
    //                 });
    //         });
    //     });
};

module.exports = service;
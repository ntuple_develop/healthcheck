'use strict';

const process = require('process');
const fs = require('fs');
const logger = require('../utils/logger');

const CONFIG = {};

// 보안정보가 포함되어있는 파일 경로
// CONFIG.ENV_INFO_FILE_PATH = 'C:/env/healthcheck.json';
CONFIG.ENV_INFO_FILE_PATH = 'C:/git/healthcheck/docs/healthcheck.json';
// ENV_INFO_FILE_PATH 경로의 json 파일 정보를 가지고 있음
CONFIG.ENV_INFO = null;

// API prefix URL
CONFIG.API_PREFIX_URL = '/api';

// 로그파일 이름
CONFIG.LOG_FILE_NAME = 'app.log';

// 로그파일 max size
CONFIG.LOG_MAX_FILE_SIZE = 10485760;

// 로그파일 rolling 기준 파일 갯수
CONFIG.LOG_MAX_FILE_COUNT = 3;

// 초기 테스트 데이터 insert 여부
CONFIG.ENABLE_TEST_DATA_INSERT = false;

try {
    // 파일 체크
    let ENV_FILE_INFO_STRING = fs.readFileSync(CONFIG.ENV_INFO_FILE_PATH, 'utf8');
    let ENV_INFO = JSON.parse(ENV_FILE_INFO_STRING);
    CONFIG.ENV_INFO = ENV_INFO;
} catch (error) {
    logger.error(CONFIG.ENV_INFO_FILE_PATH + ' file not found');
    process.exit(-1);
}

// slack notification prefix url
CONFIG.SLACK_NOTIFICATION_PREFIX_URL = 'https://ntuple.slack.com/services/hooks/slackbot';

// HTTP TIMEOUT MS
CONFIG.TIMEOUT_MAX_MS = 15000;

// init json file path
CONFIG.DATAIMPORT_JSON_FILE_PATH = 'C:/git/healthcheck/docs/korail.json';

// 모니터링 realtime cron string : 5분마다(*/5 * * * *)
CONFIG.MONITORING_REAL_SCHE_CRON_STRING = '*/30 * * * * *';

// 모니터링 daily cron string : 매일 오전 7시 마다
CONFIG.MONITORING_DAILY_SCHE_CRON_STRING = '0 7 * * *';

// 실시간 모니터링 error count 기준 : 값이 3일 경우 3번 연속으로 실패시 알림을 보냄
CONFIG.NOTI_CHECK_ERROR_COUNT = 3;

// 최초 요청시 에러일 경우 NOTI_CHECK_ERROR_COUNT 값을 기준으로 연속으로 실패할 경우에 slack으로 알림을 보내는데 이때 요청 시간 간격 : 10초(10000)
CONFIG.ERROR_REQUEST_CALL_INTERVAL_TIMEMS = 10000;

module.exports = CONFIG;
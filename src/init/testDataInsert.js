'use strict';

const dbService = require('../services/db');
const STATIC_CONFIG = require('../config/static');

module.exports = function () {

    let projectInfo = {
        id: 1,
        name: '코레일',
        slack_channel_name: '#healthcheck'
    };

    let taskList = [
        {
            project_id: 1, url: 'http://127.0.0.1/api/test/timeout', request_type: STATIC_CONFIG.REQUEST_TYPE_RESTAPI,
            monitoring_type: STATIC_CONFIG.MONITORING_TYPE_REAL
        },
        {
            project_id: 1, url: 'http://127.0.0.1/api/test/error', request_type: STATIC_CONFIG.REQUEST_TYPE_RESTAPI,
            monitoring_type: STATIC_CONFIG.MONITORING_TYPE_REAL
        },
        {
            project_id: 1, url: 'http://127.0.0.1/api/test/success', request_type: STATIC_CONFIG.REQUEST_TYPE_RESTAPI,
            monitoring_type: STATIC_CONFIG.MONITORING_TYPE_REAL
        },
        {
            project_id: 1, url: 'http://127.0.0.1/api/test/successFail', request_type: STATIC_CONFIG.REQUEST_TYPE_RESTAPI,
            monitoring_type: STATIC_CONFIG.MONITORING_TYPE_REAL
        },
        {
            project_id: 1, url: 'http://127.0.0.1/api/test/errorCount', request_type: STATIC_CONFIG.REQUEST_TYPE_RESTAPI,
            monitoring_type: STATIC_CONFIG.MONITORING_TYPE_REAL
        },
        {
            project_id: 1, url: 'http://127.0.0.1/api/test/jsonResponseTest1', request_type: STATIC_CONFIG.REQUEST_TYPE_RESTAPI,
            monitoring_type: STATIC_CONFIG.MONITORING_TYPE_DAILY,
            json_response_skeleton: {
                value1: 0,
                value2: '1',
                value3: true,
                a: {},
                b: {
                    b1: '',
                    b2: 10
                }
            }
        },
        {
            project_id: 1, url: 'http://127.0.0.1/api/test/jsonResponseTest2', request_type: STATIC_CONFIG.REQUEST_TYPE_RESTAPI,
            monitoring_type: STATIC_CONFIG.MONITORING_TYPE_DAILY,
            json_response_skeleton: {
                totalCout: 10,
                success: true,
                data: [],
                detailInfo: {
                    d1: 'd1',
                    d2: 2
                }
            }
        },
        {
            project_id: 1, url: 'http://127.0.0.1/api/test/jsonResponseTest3', request_type: STATIC_CONFIG.REQUEST_TYPE_RESTAPI,
            monitoring_type: STATIC_CONFIG.MONITORING_TYPE_DAILY,
            json_response_skeleton: {
                value1: 0,
                value2: '1',
                value3: true,
                a: {},
                b: {
                    b1: '',
                    b2: 10
                }
            }
        },
        {
            project_id: 1, url: 'http://127.0.0.1/domTest.html', request_type: STATIC_CONFIG.REQUEST_TYPE_ELEMENT,
            monitoring_type: STATIC_CONFIG.MONITORING_TYPE_DAILY,
            dom_selector: '#content > div'
        }
    ];

    return dbService.insert('project', projectInfo)
        .then(() => {
            let promiseList = [];
            for (let index = 0; index < taskList.length; index++) {
                let taskInfo = taskList[index];
                if (taskInfo.json_response_skeleton) {
                    taskInfo.json_response_skeleton = JSON.stringify(taskInfo.json_response_skeleton);
                }
                promiseList.push(dbService.insert('task', taskInfo));
            }
            return Promise.all(promiseList);
        });

};
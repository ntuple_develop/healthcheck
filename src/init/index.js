'use strict';

const CONFIG = require('../config');
const logger = require('../utils/logger');
const dbService = require('../services/db');
const testDataInsert = require('./testDataInsert');

module.exports = function (app) {

    logger.info('init app : ' + app);
    dbService.connect()
        .then(() => {
            if (CONFIG.ENABLE_TEST_DATA_INSERT) {
                // test data insert
                testDataInsert();
            }
        });

};

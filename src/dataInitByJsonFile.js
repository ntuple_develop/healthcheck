const CONFIG = require('./config');
const fs = require('fs');
const logger = require('./utils/logger');
const dbService = require('./services/db');

let DATAIMPORT_FILE_INFO_STRING = null;
let DATAIMPORT_INFO = null;

try {
    // 파일 체크
    DATAIMPORT_FILE_INFO_STRING = fs.readFileSync(CONFIG.DATAIMPORT_JSON_FILE_PATH, 'utf8');
    DATAIMPORT_INFO = JSON.parse(DATAIMPORT_FILE_INFO_STRING);
} catch (error) {
    logger.error(CONFIG.DATAIMPORT_JSON_FILE_PATH + ' file not found');
    process.exit(-1);
}

let projectInfo = DATAIMPORT_INFO.projectInfo;
let taskList = DATAIMPORT_INFO.taskList;

dbService.insert('project', projectInfo)
    .then(() => {
        let promiseList = [];
        for (let index = 0; index < taskList.length; index++) {
            let taskInfo = taskList[index];
            if (taskInfo.json_response_skeleton) {
                taskInfo.json_response_skeleton = JSON.stringify(taskInfo.json_response_skeleton);
            }
            promiseList.push(dbService.insert('task', taskInfo));
        }
        return Promise.all(promiseList);
    }).then(() => {
        process.exit(0);
    });
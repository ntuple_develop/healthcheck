'use strict';

const ErrorInstance = require('./ErrorInstance');
const uuid = require('uuid');

let errorInstanceRepository = {};

errorInstanceRepository.store = {};

// error instance create
errorInstanceRepository.create = function(taskInfo) {
    let instanceId = uuid.v4();
    errorInstanceRepository.store[instanceId] = new ErrorInstance(instanceId, taskInfo);
};

// error instance delete
errorInstanceRepository.delete = function(id) {
    if(errorInstanceRepository.store[id]) {
        delete errorInstanceRepository.store[id];
        errorInstanceRepository.store[id] = null;
    }    
};

module.exports = errorInstanceRepository;
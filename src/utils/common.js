'use strict';

const moment = require('moment');
const STATIC_CONFIG = require('../config/static');

let util = {};

util.convertSlackSendMessage = function (httpResponse, taskInfo, message) {
    let dateString = moment().format('YYYY-MM-DD HH:mm:ss');
    let projectName = taskInfo.project_name;
    let url = taskInfo.url;
    let statusCode = httpResponse.status ? httpResponse.status : (httpResponse && httpResponse.response && httpResponse.response.status);
    let errorType = '';
    let errorMessage = '';
    if (!statusCode) {
        errorType = 'timeout';
        errorMessage = httpResponse && httpResponse.message;
    } else if (statusCode === 500) {
        errorType = 'servererror';
        errorMessage = httpResponse && httpResponse.message;
    } else {
        // http 응답코드가 성공인 경우에 에러인 경우 처리
        if (taskInfo.request_type === STATIC_CONFIG.REQUEST_TYPE_ELEMENT) {
            errorType = 'element not found';
            errorMessage = taskInfo.dom_selector + ' not found';
        } else {
            // 성공이고 그외인 경우는 json 비교
            errorType = 'json response difference error';
            errorMessage = message || 'diff error';
        }
    }
    // ([시간]) [project 이름] [url] [http status code] [errorType명] : errorMessage;
    let slackSendMessage = `([${dateString}]) [${projectName}] [${url}] [${statusCode}] [${errorType}] : ${errorMessage};`;
    return slackSendMessage;

};

module.exports = util;
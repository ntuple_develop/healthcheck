'use strict';

const CONFIG = require('../config');
const axios = require('axios');
const slackService = require('../services/slack');
const commonUtil = require('../utils/common');
const errorInstanceRepository = require('./errorInstanceRepository');

module.exports = function ErrorInstance(id, taskInfo) {

    this.id = id;

    this.checkErrorCount = 0;

    this.destroy = function() {
        if(this.intervalId) {
            clearInterval(this.intervalId);
        }
        errorInstanceRepository.delete(this.id);
    };

    this.intervalId = setInterval(() => {
        this.checkErrorCount++;
        axios.get(taskInfo.url, { timeout: CONFIG.TIMEOUT_MAX_MS })
            .then(() => {
                // 성공일 경우 resource 제거
                this.destroy();
            })
            .catch((failResponse) => {
                // 실패일 경우 연속으로 몇번실패했는지 체크하기
                if(this.checkErrorCount === CONFIG.NOTI_CHECK_ERROR_COUNT) {
                    let slackSendMessage = commonUtil.convertSlackSendMessage(failResponse, taskInfo);
                    slackService.notificationToChannel(taskInfo.slack_channel_name, slackSendMessage);
                    this.destroy();
                }                
            });
    }, CONFIG.ERROR_REQUEST_CALL_INTERVAL_TIMEMS);

};
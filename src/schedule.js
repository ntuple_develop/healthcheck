const scheduleService = require('./services/schedule');
const nodeSchedule = require('node-schedule');
const process = require('process');
const logger = require('./utils/logger');
const CONFIG = require('./config');

logger.info('=================== src/schedule.js start ===================');

let jobs = [];

// 실시간 오류 점검 task
let realTimeJob = nodeSchedule.scheduleJob(CONFIG.MONITORING_REAL_SCHE_CRON_STRING, scheduleService.startMonitoringRealTime);

// daily 오류 점검 task
let dailyJob = nodeSchedule.scheduleJob(CONFIG.MONITORING_DAILY_SCHE_CRON_STRING, scheduleService.startMonitoringDaily);

jobs.push(realTimeJob);
jobs.push(dailyJob);

// 전역 promise 오류(reject) catch
process.on('unhandledRejection', (error, promise) => {
    logger.error('unhandledRejection error : ' + error);
    logger.error('unhandledRejection promise : ' + promise);
    if (error.stack) {
        logger.error('unhandledRejection stack : ' + error.stack);
    }
});

// catch all
process.on('uncaughtException', function (err) {
    logger.error('uncaughtException : ' + err);
});
'use strict';

const express = require('express');
const router = express.Router();
const AppError = require('../errors/AppError');
const errorRouteHandler = require('../errors/routeHandler');
const TIMEOUT_TEST_MS = 2000;
const TEST_ERROR_CHECK_COUNT = 5;
let currentErrorCount = 0;
let dbService = require('../services/db');

/* timeout test */
router.get('/timeout', function (req, res, next) {
    let timeoutTestPromise = new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, TIMEOUT_TEST_MS);
    });
    timeoutTestPromise.then(() => {
        res.send({ success: true });
    }).catch(errorRouteHandler(next));
});


/* success test(status code 200) */
router.get('/success', function (req, res) {
    res.send({ success: true });
});

/* error test(status code 500) */
router.get('/error', function () {
    throw new AppError('error');
});

/* successFail test(status code 200) */
router.get('/successFail', function (req, res) {
    res.send({ success: false, errorCode: '01', errorMessage: '결재정보가 올바르지 않습니다' });
});

/* errorCount test(status code 200 or 500) */
router.get('/errorCount', function (req, res) {
    currentErrorCount++;
    if (currentErrorCount < TEST_ERROR_CHECK_COUNT) {
        throw new AppError('errorCount error');
    } else {
        res.send({ success: true });
    }
});

// json 응답 골격 테스트 case 1
router.get('/jsonResponseTest1', function (req, res) {

    // json_response_skeleton: {
    //     value1: 0,
    //         value2: '1',
    //             value3: true,
    //                 a: { },
    //     b: {
    //         b1: '',
    //             b2: 10
    //     },
    // }

    res.send({
        value10: 0,
        value20: '1',
        value30: true,
        a: {},
        b: {
            b10: '',
            b23: 10
        },
    });

});

// json 응답 골격 테스트 case 2
router.get('/jsonResponseTest2', function (req, res) {

    // json_response_skeleton: {
    //     totalCout: 10,
    //         success: true,
    //             data: [],
    //                 detailInfo: {
    //         d1: 'd1',
    //             d2: 2
    //     }
    // }

    res.send({
        totalCout: 10,
        success: true,
        data: [],
        detailInfo: {
            d1: 'd1',
            d2: 2
        }
    });

});

// jsonResponseTest3 : case1에 있는 것을 array로 반환
router.get('/jsonResponseTest3', function (req, res) {

    // json_response_skeleton: {
    //     value1: 0,
    //         value2: '1',
    //             value3: true,
    //                 a: { },
    //     b: {
    //         b1: '',
    //             b2: 10
    //     },
    // }

    res.send([{
        value1: 0,
        value2: '1',
        value3: true,
        a: {},
        b: {
            b1: '',
            b2: 10
        },
    }]);

});

router.get('/taskListAll', function (req, res, next) {

    dbService.selectQueryById('getTaskListAll')
        .then((result) => {
            res.send(result);
        }).catch(errorRouteHandler(next));

});

router.get('/projects', function (req, res, next) {

    dbService.select('project')
        .then((result) => {
            res.send(result);
        }).catch(errorRouteHandler(next));

});

router.get('/tasks', function (req, res, next) {

    dbService.select('task')
        .then((result) => {
            res.send(result);
        }).catch(errorRouteHandler(next));

});


module.exports = router;
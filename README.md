## 사전 install

*   node 설치
*   node 라이브러리 설치
    *   npm install

## npm run script

| index | run name (파일명) | 설명 |
|----------|----------|-------------|-------------|
| 1 | start (__index.js__) | 테스트를 위한 서버 실행
| 2 | dataInit (__dataInit.js__) | 테스트를 위한 DB insert 실행 |
| 3 | schedule (__schedule.js__) | db에 등록된 api를 요청하는 스케줄러 job 실행 |

## npm run script 상세 설명

*   1.__start__ : 실패 테스트를 하기 위해 만든 서버

*   2.__dataInit__ : api 정보를 db table에 insert

*   3.__schedule__ : db 테이블에 저장된 api 정보를 조회해와서 ajax로 서버에 요청을 실패시 slack에 notification 하는 프로그램